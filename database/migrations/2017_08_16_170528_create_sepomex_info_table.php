<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSepomexInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sepomex_info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('state_code');
            $table->string('state_name');
            $table->string('town_code');
            $table->string('town_name');
            $table->string('neighbourhood');
            $table->string('neighbourhood_type');
            $table->string('zip_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sepomex_info');
    }
}
