<?php

use App\Http\Repositories\SepomexRepository;
use App\Models\State;
use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = SepomexRepository::states();

        foreach ($states as $state) {
            State::create([
               'code' => $state['state_code'],
               'name' => $state['state_name'],
               'slug' => str_slug($state['state_name'],'-'),
            ]);
        }
    }
}
