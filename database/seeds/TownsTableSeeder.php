<?php


use App\Models\SepomexInfo;
use App\Models\Town;
use Illuminate\Database\Seeder;

class TownsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $towns = SepomexInfo::select(['town_name', 'town_code', 'state_code'])
        ->distinct(['town_name', 'town_code', 'state_code'])
                                  ->orderBy('state_code', 'ASC')
                                  ->get();

        foreach ($towns as $town) {
            Town::create([
                'code'       => $town['town_code'],
                'name'       => $town['town_name'],
                'state_code' => $town['state_code'],
                'slug'       => str_slug($town['town_name']),
            ]);
        }
    }
}
