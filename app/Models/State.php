<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model {

    protected $fillable = ['code','name','slug'];
    protected $visible = ['code','name','slug'];


    public static $rules = [
        // Validation rules
    ];

    // Relationships

    public function zipCodes()
    {
        return $this->belongsToMany(ZipCode::class);
    }

}
