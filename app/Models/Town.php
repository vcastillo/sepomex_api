<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Town extends Model {

    protected $fillable = ['code','name','slug','state_code'];
    protected $visible = ['code','name','slug','state_code'];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships

}
