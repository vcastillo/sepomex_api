<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SepomexInfo extends Model {

    protected $fillable = ['state_code','state_name','town_code','town_name','neighborhood','neighborhood_type','zip_code'];

    protected $hidden = ['updated_at','created_at','neighborhood_type','id'];


    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    protected $table = 'sepomex_info';

    // Relationships

}
