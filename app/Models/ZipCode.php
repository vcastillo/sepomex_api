<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ZipCode extends Model {

    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships


    public function states()
    {
        return $this->hasMany(State::class);
    }

    public function towns()
    {
        return $this->hasMany(Town::class);
    }
}
