<?php

namespace App\Http\Controllers;

use App\Helpers\HttpCodes;
use App\Http\Repositories\SepomexRepository;
use App\Models\SepomexInfo;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class SepomexController extends Controller
{
     const DOUBLEP = '::';
     const BASE_NAMESPACE_REP = "";
     protected $_repository;


    /**
     * SepomexController constructor.
     */
    public function __construct()
    {
        $this->_repository = 'SepomexRepository';
    }

    public function searchByZipCode($zipCode, $limit = SepomexRepository::LIMIT_RECORDS, $neighborhood = true)
    {
        return $this->callMethod('getByCode', $zipCode, $limit, $neighborhood);
//      return SepomexRepository::getByCode($zipCode);
    }

    public function states()
    {
        return $this->callMethod('states');
//        return SepomexRepository::states();
    }

    public function towns($stateCode)
    {
        return $this->callMethod('towns', $stateCode);
//        return SepomexRepository::towns($stateCode);
    }

    public function neighborhoods($stateCode,$townCode)
    {
        return $this->callMethod('neighborhood', $stateCode, $townCode);
//        return SepomexRepository::neighborhood($stateCode,$townCode);
    }

    public function searchZipCodeByNeighborhood($stateCode,$townCode,$neighborhood)
    {
        return $this->callMethod('zipCode', $stateCode, $townCode, $neighborhood);
//        return SepomexRepository::zipCode($stateCode,$townCode,$neighborhood);
    }

    public function getState($stateCode)
    {
//        $this->_repository = 'StateRepository';
        return $this->callMethod('getState',$stateCode);
//        return SepomexRepository::getState($stateCode);
    }

    public function getTown($stateCode,$townCode)
    {
        return $this->callMethod('getTown',$stateCode,$townCode);
//        return SepomexRepository::getTown($stateCode,$townCode);
    }

    public function getStateBySlug($slug)
    {
        $this->_repository = 'StateRepository';
        return $this->callMethod('getBySlug',$slug);
    }

    public function getTownBySlug($stateCode,$slug)
    {
        $this->_repository = 'TownRepository';
        return $this->callMethod('getBySlug',$stateCode,$slug);
    }

    /**
     * @param mixed $data
     * @param $statusCode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function decodeResponse($data, $statusCode)
    {
        $header = array(
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset'      => 'utf-8',
        );

        if(empty($data->toArray())){
            $statusCode = HttpCodes::NO_CONTENT;
            $data['message'] = 'No results';
        }

        return response()->json($data, $statusCode, $header, JSON_UNESCAPED_UNICODE);
    }


    private function callMethod($method,...$arguments)
    {
        $statusCode = HttpCodes::SUCCESS;
        try {
            $repository = REPOSITORY_NAMESPACE.$this->_repository.DOUBLE_DOT;
            $data = forward_static_call_array($repository . $method, $arguments);
        } catch (\Exception $exception) {
            $statusCode = HttpCodes::INTERNAL_SERVER_ERROR;
            $data       = [
                'success' => false,
                'message' => "Something went wrong, Can't get the data",
                '_e'      => [
                    'error' => $exception->getMessage(),
                    'code'  => $exception->getCode(),
                ],
            ];
        }

        return $this->decodeResponse($data, $statusCode);
    }
}
