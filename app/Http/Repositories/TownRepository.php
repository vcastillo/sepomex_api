<?php
/**
 * Created by PhpStorm.
 * User: aleattov
 * Date: 10/26/17
 * Time: 5:43 PM
 */

namespace App\Http\Repositories;


use App\Models\Town;

class TownRepository
{
    static function getBySlug($stateCode,$slug)
    {
        return Town::where('state_code',$stateCode)
            ->where('slug','like',"%$slug%")
            ->firstOrFail();
    }
}