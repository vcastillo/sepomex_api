<?php
/**
 * Created by PhpStorm.
 * User: aleattov
 * Date: 10/26/17
 * Time: 3:38 PM
 */

namespace App\Http\Repositories;


use App\Models\State;

class StateRepository
{

    static function getBySlug($slug)
    {
        return State::where('slug', 'like', "%$slug%")
                    ->orWhere('name', 'like', "%$slug%")
                    ->first();
    }

    static function getByCode($code)
    {
        return State::where('code',$code)->firstOrFail();
    }
}