<?php

namespace App\Http\Repositories;

use App\Models\SepomexInfo;
use Illuminate\Support\Facades\Log;


class SepomexRepository
{
    const LIMIT_RECORDS = 10;

    static function getByCode($code, $limit = SepomexRepository::LIMIT_RECORDS, $neighborhood = true )
    {
        $fields = [
            'zip_code',
            'state_code',
            'state_name',
            'town_code',
            'town_name'
        ];

        $includeNeighborhood = filter_var($neighborhood,FILTER_VALIDATE_BOOLEAN);

        if ($includeNeighborhood) {
            $fields[] = 'neighborhood';
        }

        $stringCode = '%' . $code . '%';
        $records    = SepomexInfo::where('zip_code', 'like', $stringCode)
                                 ->select($fields)
                                 ->limit($limit)
                                 ->get();

        return $records;

    }

    static function states()
    {
        return SepomexInfo::select(['state_name','state_code'])
                          ->distinct(['state_name','state_code'])
                            ->orderBy('state_code','ASC')
                          ->get();
    }

    static function towns($stateCode)
    {
        return SepomexInfo::select(['town_name','town_code'])
                          ->distinct(['town_name','town_code'])
                          ->orderBy('town_code','ASC')
                            ->where('state_code',$stateCode)
                          ->get();
    }

    static function neighborhood($stateCode,$townCode)
    {
        return SepomexInfo::select(['neighborhood'])
                        ->orderBy('neighborhood', 'ASC')
                        ->groupBy('neighborhood')
                        ->where('town_code',$townCode)
                        ->where('state_code',$stateCode)
                        ->get();
    }

    static function zipCode($stateCode,$townCode,$neighborhood)
    {
        return SepomexInfo::select(['zip_code'])
                        ->orderBy('zip_code','ASC')
                        ->where('state_code',$stateCode)
                        ->where('town_code',$townCode)
                        ->where('neighborhood',$neighborhood)
                        ->get();
    }

    static function getState($stateCode)
    {
        return SepomexInfo::select(['state_code','state_name'])
                    ->limit(1)
                    ->where('state_code',$stateCode)
                    ->get();
    }

    static function getTown($stateCode, $townCode)
    {
        return SepomexInfo::select(['town_code','town_name'])
                ->limit(1)
                ->where('state_code',$stateCode)
                ->where('town_code',$townCode)
                ->get();
    }
}