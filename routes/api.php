<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::prefix('v1')->middleware(['client'])->group(function(){
    Route::get('/search/code/{zipcode}','SepomexController@searchByZipCode');
    Route::get('/search/code/{zipcode}/limit/{number?}','SepomexController@searchByZipCode');
    Route::get('/search/code/{zipcode}/limit/{number?}/neighborhood/{neighborhood?}','SepomexController@searchByZipCode');

    Route::get('/states/','SepomexController@states');
    Route::get('/state/{stateCode}/towns','SepomexController@towns');
    Route::get('/state/{stateCode}/town/{townCode}/neighborhood','SepomexController@neighborhoods');
    Route::get('/state/{stateCode}/town/{townCode}/neighborhood/{neighborhood}/code','SepomexController@searchZipCodeByNeighborhood');
    Route::get('/state/{stateCode}','SepomexController@getState');
    Route::get('/state-slug/{slug}','SepomexController@getStateBySlug');
    Route::get('/state/{stateCode}/town/{townCode}','SepomexController@getTown');
    Route::get('/state/{stateCode}/town-slug/{slug}','SepomexController@getTownBySlug');
});
